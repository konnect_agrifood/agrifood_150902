//
//  CustomCell.swift
//  AgriFood_150902
//
//  Created by 石田渡 on 2015/09/02.
//  Copyright (c) 2015年 石田渡. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {
    @IBOutlet weak var outletSource: UILabel!
    @IBOutlet weak var outletDate: UILabel!
    @IBOutlet weak var outletTitle: UILabel!
    @IBOutlet weak var outletDescription: UILabel!
    @IBOutlet weak var outletImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    func setCellSource(source: String) {
        self.outletSource.text = source as String
    }
    
    func setCellDate(date: String) {
        self.outletDate.text = date as String
    }
    
    func setCellTitle(title: String) {
        self.outletTitle.text = title as String
    }
    
    func setCellDescription(description: String) {
        let text = description as String
        let attributedText = NSMutableAttributedString(string: text)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4
        paragraphStyle.lineHeightMultiple = 1
        attributedText.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSMakeRange(0, attributedText.length))
        self.outletDescription.attributedText = attributedText
    }
    
    func setCellImage(url: String){
        var err: NSError?
        var imageData :NSData = NSData(contentsOfURL: NSURL(string: url)!,options: NSDataReadingOptions.DataReadingMappedIfSafe, error: &err)!
        self.outletImage.image = UIImage(data:imageData)
    }
}
