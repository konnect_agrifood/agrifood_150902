//
//  makeTableData.swift
//  AgriFood_150902
//
//  Created by 石田渡 on 2015/09/04.
//  Copyright (c) 2015年 石田渡. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class makeTableData: NSObject {
    private var url:String
    private var tag:String
    private var sourceItems = NSMutableArray()
    private var dateItems = NSMutableArray()
    private var titleItems = NSMutableArray()
    private var descriptionItems = NSMutableArray()
    private var imageUrlItems = NSMutableArray()
    private var urlItems = NSMutableArray()
    private var cellNum = 30
    private var isInLoad = false
    
    init(url:String, tag:String){
        self.url = url
        self.tag = tag
    }
    
    func setData(){
        isInLoad = true
        var index = 0
        var task = NSURLSession.sharedSession().dataTaskWithURL(NSURL(string: self.url)!, completionHandler: {data, response, error in
            var json = JSON(data: data)
            let now = NSDate()
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
            switch self.tag{
                case "source":
                    for var i = 0; i < self.cellNum; i++ {
                        self.sourceItems[i] = json[i]["source"].stringValue
                    }
                case "date":
                    for var i = 0; i < self.cellNum; i++ {
                        var date = json[i]["date"].stringValue
                        let yyyy_date = (date as NSString).substringWithRange(NSRange(location: 0, length: 4))
                        let MM_date = (date as NSString).substringWithRange(NSRange(location: 4, length: 2))
                        let dd_date = (date as NSString).substringWithRange(NSRange(location: 6, length: 2))
                        let HH_date = (date as NSString).substringWithRange(NSRange(location: 8, length: 2))
                        let mm_date = (date as NSString).substringWithRange(NSRange(location: 10, length: 2))
                        date.insert("/", atIndex:advance(date.startIndex, 4))
                        date.insert("/", atIndex:advance(date.startIndex, 7))
                        date.insert(" ", atIndex:advance(date.startIndex, 10))
                        date.insert(":", atIndex:advance(date.startIndex, 13))
                        date.insert(":", atIndex:date.endIndex)
                        date += "00"
                        let date1:NSDate? = dateFormatter.dateFromString(date)
                        let calendar = NSCalendar.currentCalendar()
                        let calUnit:NSCalendarUnit = .CalendarUnitMinute | .CalendarUnitHour | .CalendarUnitDay
                        var comp: NSDateComponents = calendar.components(calUnit, fromDate: date1!, toDate: now, options:nil)
                        let day_diff = comp.day
                        let hour_diff = comp.hour
                        let minute_diff = comp.minute
                        if day_diff != 0 && day_diff != 1 {
                            self.dateItems[i] = yyyy_date + "/" + MM_date + "/" + dd_date
                        }else if day_diff == 1 {
                            self.dateItems[i] = "昨日 " + HH_date + ":" + mm_date
                        }else if day_diff == 0 && hour_diff != 0 {
                            self.dateItems[i] = hour_diff.description + "時間前"
                        }else if day_diff == 0 && hour_diff == 0 {
                            self.dateItems[i] = minute_diff.description + "分前"
                        }
                    }
                case "title":
                    for var i = 0; i < self.cellNum; i++ {
                        self.titleItems[i] = json[i]["title"].stringValue
                    }
                case "description":
                    for var i = 0; i < self.cellNum; i++ {
                        self.descriptionItems[i] = json[i]["description"].stringValue
                    }
                case "imageUrl":
                    for var i = 0; i < self.cellNum; i++ {
                        self.imageUrlItems[i] = json[i]["image"].stringValue
                    }
                case "url":
                    for var i = 0; i < self.cellNum; i++ {
                        self.urlItems[i] = json[i]["url"].stringValue
                    }
                default:
                    break
                
            }
            self.isInLoad = false
        })
        task.resume()
        while isInLoad {
            usleep(10)
            index++
        }
    }

    subscript(num:Int) -> String{
        switch self.tag{
            case "source":
                return self.sourceItems[num] as! String
            case "date":
                return self.dateItems[num] as! String
            case "title":
                return self.titleItems[num] as! String
            case "description":
                return self.descriptionItems[num] as! String
            case "imageUrl":
                return self.imageUrlItems[num] as! String
            case "url":
                return self.urlItems[num] as! String
            default:
                return "えらー"
        }
    }
   
}
