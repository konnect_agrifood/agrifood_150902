//
//  FirstViewController.swift
//  AgriFood_150902
//
//  Created by 石田渡 on 2015/09/01.
//  Copyright (c) 2015年 石田渡. All rights reserved.
//

import UIKit
import SwiftyJSON

class FirstViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    // 統合できないか、、、？（title/source/date/description/imageUrl/url)
    var titleData = makeTableData(url: "http://agrifood.jp/allArticles.json", tag: "title")
    var sourceData = makeTableData(url: "http://agrifood.jp/allArticles.json", tag: "source")
    var dateData = makeTableData(url: "http://agrifood.jp/allArticles.json", tag: "date")
    var descriptionData = makeTableData(url: "http://agrifood.jp/allArticles.json", tag: "description")
    var imageUrlData = makeTableData(url: "http://agrifood.jp/allArticles.json", tag: "imageUrl")
    var urlData = makeTableData(url: "http://agrifood.jp/allArticles.json", tag: "url")
    var refresh = UIRefreshControl()
    var selectedRow: Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        setTableData()
        addRefreshControl()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = 90
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    func setTableData(){
        titleData.setData()
        sourceData.setData()
        dateData.setData()
        imageUrlData.setData()
        descriptionData.setData()
        urlData.setData()
        refresh.endRefreshing()
    }
    
    func addRefreshControl() {
        refresh.addTarget(self, action: "setTableData", forControlEvents: UIControlEvents.ValueChanged)
        tableView.addSubview(refresh)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 30
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath:NSIndexPath) -> UITableViewCell {
        let cell: CustomCell = tableView.dequeueReusableCellWithIdentifier("CustomCell", forIndexPath: indexPath) as! CustomCell
        cell.setCellTitle(titleData[indexPath.row])
        cell.setCellSource(sourceData[indexPath.row])
        cell.setCellImage(imageUrlData[indexPath.row])
        cell.setCellDescription(descriptionData[indexPath.row])
        cell.setCellDate(dateData[indexPath.row])
        return cell
    }
    
    // CellViewControllerにセルの値を渡す
    func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        self.selectedRow = indexPath.row
        performSegueWithIdentifier("toCellViewController", sender: nil)
        return nil
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "toCellViewController" {
            let cellVC : CellViewController = segue.destinationViewController as! CellViewController
            cellVC.articleurl = urlData[self.selectedRow!]
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
    