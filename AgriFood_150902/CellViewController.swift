//
//  CellViewController.swift
//  AgriFood_150902
//
//  Created by 石田渡 on 2015/09/03.
//  Copyright (c) 2015年 石田渡. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class CellViewController: UIViewController {
    var articleurl : String?
    var webview : WKWebView?
    let deviceBound : CGRect = UIScreen.mainScreen().bounds
    
    // 戻るボタンの実装
    // 戻る機能（byジェスチャー）の実装
    
    override func loadView() {
        super.loadView()
        self.webview = WKWebView(frame: CGRectMake(0, 20, deviceBound.size.width, deviceBound.size.height - 20))
        self.view.addSubview(self.webview!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let requestURL = NSURL(string: articleurl!)
        let req = NSURLRequest(URL: requestURL!)
        webview!.loadRequest(req)
        self.webview?.addObserver(self, forKeyPath:"estimatedProgress", options:.New, context:nil)
        // self.webview?.allowsBackForwardNavigationGestures = true
        // var swipeRight = UISwipeGestureRecognizer(target: self, action: "dismissView:")
        // swipeRight.direction = .Right
        // webview!.addGestureRecognizer(swipeRight)
    }
    
    override func observeValueForKeyPath(keyPath:String, ofObject object:AnyObject, change:[NSObject:AnyObject], context:UnsafeMutablePointer<Void>) {
        switch keyPath {
        case "estimatedProgress":
            var myProgressView: UIProgressView = UIProgressView(frame: CGRectMake(0, 20, deviceBound.size.width, 10))
            if let progress = change[NSKeyValueChangeNewKey] as? Float {
                println("Progress:\(progress)")
                myProgressView.progressTintColor = UIColor.whiteColor()
                myProgressView.trackTintColor = UIColor.lightGrayColor()
                
                // ここでprogressを料理
                
                myProgressView.progress = progress
                // myProgressView.progress = progress_cooked
                self.view.addSubview(myProgressView)
            }
        default:
            break
        }
    }
    
    deinit {
        self.webview?.removeObserver(self, forKeyPath: "estimatedProgress")
    }
    
    // func dismissView(sender: UITapGestureRecognizer){
        // print("ジェスチャーテスト")
        // self.dismissViewControllerAnimated(true, completion: nil)
    // }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}